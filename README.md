# React Router

## Sub-Items

- Frontend SPA (dev server)
- Backend API (api server)

## Install Dependencies

- cd to frontend directory `npm install`
- cd to backend directory `npm install`

## How to Run

- cd to frontend directory `npm start`
- cd to backend directory `npm start`
